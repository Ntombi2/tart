%% Calculating the Dielectric permittivity constant

W=25*10^-3;
L=25*10^-3;
h=4*10^-3;
f=1.57542*10^9;
mu = 4*pi*10^-7;
epsilon= 8.854*10^-12;
velocity= 1/(sqrt(mu*epsilon));

e_r = 2*(velocity/2*W*f)^2 -1
e_reff = (e_r +1)/2 + ((e_r-1)/2)*(1+12* h/W)
