function GPSPassivePatchbeam6 = importfile12(filename, startRow, endRow)
%IMPORTFILE12 Import numeric data from a text file as a matrix.
%   GPSPASSIVEPATCHBEAM6 = IMPORTFILE12(FILENAME) Reads data from text file
%   FILENAME for the default selection.
%
%   GPSPASSIVEPATCHBEAM6 = IMPORTFILE12(FILENAME, STARTROW, ENDROW) Reads
%   data from rows STARTROW through ENDROW of text file FILENAME.
%
% Example:
%   GPSPassivePatchbeam6 = importfile12('GPS_PassivePatch_beam6.txt', 2, 65342);
%
%    See also TEXTSCAN.

% Auto-generated by MATLAB on 2018/08/07 09:48:08

%% Initialize variables.
if nargin<=2
    startRow = 2;
    endRow = 65342;
end

%% Format for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column3: double (%f)
%	column4: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%9f%10f%10f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
textscan(fileID, '%[^\n\r]', startRow(1)-1, 'WhiteSpace', '', 'ReturnOnError', false);
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', '', 'WhiteSpace', '', 'TextType', 'string', 'EmptyValue', NaN, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    textscan(fileID, '%[^\n\r]', startRow(block)-1, 'WhiteSpace', '', 'ReturnOnError', false);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', '', 'WhiteSpace', '', 'TextType', 'string', 'EmptyValue', NaN, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Create output variable
GPSPassivePatchbeam6 = table(dataArray{1:end-1}, 'VariableNames', {'Thetadeg','Phideg','Amp','PhasePpolDatastartsonnextline'});

