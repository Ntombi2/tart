%% Co-pol
%CUT #1: P-pol amp values for THETA -180 to 180 for PHI = 0 deg
%CUT #2: P-pol amp values for THETA -180 to 180 for PHI = 90 deg
 GPSPassivePatchbeam6 = importfile12('GPS_PassivePatch_beam6.txt', 2, 65342);
 Theta=zeros;
 Theta90=zeros;
 Amp=zeros;
 Amp90=zeros;
 t=1;
 x=1;
 for n=(1:65341)
     if (GPSPassivePatchbeam6{n,2}==0 && GPSPassivePatchbeam6{n,1} > -180 && GPSPassivePatchbeam6{n,1}<180)
        Theta(t)=GPSPassivePatchbeam6{n,1};
        Amp(t)=GPSPassivePatchbeam6{n,3};
        t=t+1;
     elseif (GPSPassivePatchbeam6{n,2}==90 && GPSPassivePatchbeam6{n,1} > -180 && GPSPassivePatchbeam6{n,1}<180)
        Theta90(x)=GPSPassivePatchbeam6{n,1};
        Amp90(x)=GPSPassivePatchbeam6{n,3};
        x=x+1;
     end
 end
 plot(Theta,Amp);
 figure;
 plot(Theta90,Amp90);
%% X-pol
%CUT #3: X-pol amp values for THETA -180 to 180 for PHI = 0 deg
%CUT #4: X-pol amp values for THETA -180 to 180 for PHI = 90 deg

GPSPassivePatchbeam1 = importfile13('GPS_PassivePatch_beam6.txt', 65344, 130684);
X_Theta=zeros;
X_Theta90=zeros;
X_Amp=zeros;
X_Amp90=zeros;
 t=1;
 x=1;
  for n=(1:65341)
     if (GPSPassivePatchbeam1{n,2}==0 && GPSPassivePatchbeam1{n,1} > -180 && GPSPassivePatchbeam1{n,1}<180)
        X_Theta(t)=GPSPassivePatchbeam1{n,1};
        X_Amp(t)=GPSPassivePatchbeam1{n,3};
        t=t+1;
     elseif (GPSPassivePatchbeam1{n,2}==90 && GPSPassivePatchbeam1{n,1} > -180 && GPSPassivePatchbeam1{n,1}<180)
        X_Theta90(x)=GPSPassivePatchbeam1{n,1};
        X_Amp90(x)=GPSPassivePatchbeam1{n,3};
        x=x+1;
     end
  end
figure;
plot(X_Theta,X_Amp);
%hold;
figure;
plot(X_Theta90,X_Amp90);