% A-GPS Patch antenna
% B-NSI Antenna
% C-BioCone Antenna
%% Read measured data form .csv files
S2125BiconeNSI = importfile5('S21_25_Bicone_NSI.csv', 8, 209);
S2125PasPatch0Bicone = importfile6('S21_25_PasPatch0_Bicone.csv', 8, 209);
S2125PasPatch0NSI = importfile7('S21_25_PasPatch0_NSI.csv', 8, 209);
S2125PasPatch90Bicone = importfile8('S21_25_PasPatch90_Bicone.csv', 8, 209);
S2125PasPatch90NSI = importfile9('S21_25_PasPatch90_NSI.csv', 8, 209);
%% Distance between antennas
Height_A = 0.619;
Height_B = 0.620;
Height_C = 0.455;
Distance = 1.423 +Height_B;

R_AB = Distance- Height_A-Height_B;
R_AC = Distance- Height_A-Height_C;
R_BC = Distance- Height_C-Height_B;
%% Determine the Free space loss
mu = 4*pi*10^-7;
epsilon= 8.854*10^-12;
freq=S2125BiconeNSI.freq(101);
Lambda = 1/(sqrt(mu*epsilon)*freq);

Loss_AB = 20*log10((4*pi*R_AB)/Lambda);
Loss_AC = 20*log10((4*pi*R_AC)/Lambda);
Loss_BC = 20*log10((4*pi*R_BC)/Lambda);

% Lambda = 1./(sqrt(mu*epsilon).*freq);
% Loss_AB = 20*log10((4*pi*R_AB)./Lambda);
% Loss_AC = 20*log10((4*pi*R_AC)./Lambda);
% Loss_BC = 20*log10((4*pi*R_BC)./Lambda);
%% Determine the Gain(dB) at 0 degree
M=[1 1 0; 1 0 1; 0 1 1];
Gain_AB= Loss_AB+S2125PasPatch0NSI.S21(101);
Gain_AC= Loss_AC+S2125PasPatch0Bicone.S21(101);
Gain_BC= Loss_BC+S2125BiconeNSI.S21(101);
Gain_ABC= [Gain_AB;Gain_AC;Gain_BC];
Gain_0= M\Gain_ABC;
%% Determine the Gain(dB) at 90 degrees
Gain_AB= Loss_AB+S2125PasPatch90NSI.S21(101);
Gain_AC= Loss_AC+S2125PasPatch90Bicone.S21(101);
Gain_BC= Loss_BC+S2125BiconeNSI.S21(101);
Gain_ABC= [Gain_AB;Gain_AC;Gain_BC];
Gain_90= M\Gain_ABC;
%% Determine the circular Gain(dB)
%First will convert the DB gains at 0 and 90 degrees to linear and get the
%sum of the two gain
g_0= 10^(Gain_0(1)/20);
g_90= 10^(Gain_90(1)/20);
g_tot= sqrt(g_0^2 +g_90^2);
G_tot=20* log10(g_tot);
%% %% Plot the Gain
Gain_0=zeros;
Gain_90 = zeros;
G_tot=zeros;
Freq=zeros;

for n=(1:1:201)
  freq=S21Patch0NSI.freq(n);
  Lambda = 1/(sqrt(mu*epsilon)*freq);
  Loss_AB = 20*log10((4*pi*R_AB)/Lambda);
  Loss_AC = 20*log10((4*pi*R_AC)/Lambda);
  Loss_BC = 20*log10((4*pi*R_BC)/Lambda);

  M=[1 1 0; 1 0 1; 0 1 1];
  Gain_AB= Loss_AB+S2125PasPatch0NSI.S21(101);
  Gain_AC= Loss_AC+S2125PasPatch0Bicone.S21(101);
  Gain_BC= Loss_BC+S2125BiconeNSI.S21(101);
  Gain_ABC= [Gain_AB;Gain_AC;Gain_BC];
  G=M\Gain_ABC;
  Gain_0(n)= G(1);

  Gain_AB= Loss_AB+S2125PasPatch90NSI.S21(101);
  Gain_AC= Loss_AC+S2125PasPatch90Bicone.S21(101);
  Gain_BC= Loss_BC+S2125BiconeNSI.S21(101);
  Gain_ABC= [Gain_AB;Gain_AC;Gain_BC];
  G1=M\Gain_ABC;
  Gain_90(n)= G1(1);

  g_0= 10^(G(1)/20);
  g_90= 10^(G1(1)/20);
  g_tot= sqrt(g_0^2 +g_90^2);
  G_tot(n)=20* log10(g_tot);
end
for x=(1:1:201)
    Freq(x)=S2125BiconeNSI.freq(x);
end
 figure; 
plot(Freq,Gain_0,'g',Freq,Gain_90,'b',Freq,G_tot,'r')
xlim([1.06*10^9 2*10^9])
ylim auto
title('my title')
xlabel('Frequency (Hz)');
ylabel('Gain (dB)');
legend('Gain at 0 degrees','Gain at 90 degrees','circular Gain')

%%  Plot the Axial Ratio
ARGPSPassive = importfile11('AR_GPS_Passive.dat', 1, 11);
figure;
plot(ARGPSActive.Freq,ARGPSActive.Ratio);
xlabel('Frequency (GHz)');
ylabel('Axial Ratio (dB)');
xlim auto
ylim auto
